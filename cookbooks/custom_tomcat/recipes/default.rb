#
# Cookbook Name:: custom_tomcat
# Recipe:: default
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
include_recipe 'java'

group 'tomcat'

user 'tomcat' do
group 'tomcat'
system true
shell '/bin/bash'
end

download_url = 'http://mirror.cc.columbia.edu/pub/software/apache/tomcat/tomcat-7/v7.0.78/bin/apache-tomcat-7.0.78.tar.gz'

ark 'tomcat' do
url download_url
home_dir '/opt/tomcat'
owner 'tomcat'
group 'tomcat'
end

template 'etc/init.d/tomcat' do
source 'tomcat-init.erb'
mode '0755'
owner 'root'
group 'root'
end

execute 'chmod' do
command 'chmod 755 /opt/tomcat/bin/*.sh'
action :run
end

execute 'chkconfig' do
command 'chkconfig tomcat on'
command 'service tomcat start'
action :run
end 

